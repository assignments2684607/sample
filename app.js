// const exp = require('constants')
const express = require('express')
const app = express()
const port = 8000

app.get('/',(req,res)=>{
    res.send("This is simple  CICD file.")
})

app.listen(port,()=>{
    console.log('Application is listening at https://localhost:${port}')
})
